# beginning of models.py
# note that at this point you should have created "statesdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgres://postgres:sahay@localhost:5432/statesdb')



# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class statePlaceholder(object):
    def __init__(
        self,
        name=None,
        positive=None,
        negative=None,
        hospitalized=None,
        deaths=None,
        stayAtHomeOrder=None,
        mandatoryQuarantineForTravelers=None,
        nonEssentialBusinessClosures=None,
        largeGatheringsBan=None,
        stateMandatedSchoolClosures=None,
        barRestaurantLimits=None,
        primaryElectionPostponement=None,
        emergencyDeclaration=None,
        numberOfLaws=0
    ):
        self.name = name
        self.positive = positive
        self.negative = negative
        self.hospitalized = hospitalized
        self.deaths = deaths
        self.stayAtHomeOrder = stayAtHomeOrder
        self.mandatoryQuarantineForTravelers = mandatoryQuarantineForTravelers
        self.nonEssentialBusinessClosures = nonEssentialBusinessClosures
        self.largeGatheringsBan = largeGatheringsBan
        self.stateMandatedSchoolClosures = stateMandatedSchoolClosures
        self.barRestaurantLimits = barRestaurantLimits
        self.primaryElectionPostponement = primaryElectionPostponement
        self.emergencyDeclaration = emergencyDeclaration
        self.numberOfLaws = numberOfLaws


class State(db.Model):
    __tablename__ = 'states'

    state_name = db.Column(db.String(80), nullable=False, primary_key=True)
    positive = db.Column(db.Integer)
    negative = db.Column(db.Integer)
    hospitalized = db.Column(db.Integer)
    deaths = db.Column(db.Integer)
    stay_at_home_order = db.Column(db.String(80))
    mandatory_quarantine_for_travelers = db.Column(db.String(80))
    non_essential_business_closures = db.Column(db.String(80))
    large_gatherings_ban = db.Column(db.String(80))
    state_mandated_school_closures = db.Column(db.String(80))
    bar_restaurant_limits = db.Column(db.String(80))
    primary_election_postponement = db.Column(db.String(80))
    emergency_declaration = db.Column(db.String(80))
    number_of_laws = db.Column(db.Integer)
    counties = relationship('County', lazy=True)


class County(db.Model):
    __tablename__ = 'counties'

    id = db.Column(db.Integer, db.Sequence(
        'stats_id_seq', increment=1), primary_key=True)
    county_name = db.Column(db.String(80), nullable=False)
    state = db.Column(db.String(80), db.ForeignKey(
        'states.state_name'), nullable=False, primary_key=True)
    fips = db.Column(db.Integer)
    cases = db.Column(db.Integer)
    deaths = db.Column(db.Integer)


db.drop_all()
db.create_all()
# End of models.py
