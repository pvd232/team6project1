# beginning of create_db.py
import json, csv
import os
from models import app, db, State, County, statePlaceholder
import requests
from datetime import datetime

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


def create_states():
    halfStates = []
    states = load_json("statesdata.json")
    laws = load_json("lawsdata.json")
    for state in states:

        name = state['state']
        if name == '-':
            name = None

        positive = state['positive']

        if positive == '-':
            positive = None
        negative = state['negative']

        if negative == '-':
            negative = None
        
        try: hospitalized = state['hospitalized']
        except KeyError:
            hospitalized = None

        if hospitalized == '-':
            hospitalized = None

        try: deaths = state['death']
        except KeyError:
            deaths = None

        if deaths == '-':
            deaths = None

        halfState = statePlaceholder(name=name,
                                     positive=positive,
                                     negative=negative,
                                     hospitalized=hospitalized,
                                     deaths=deaths)
        halfStates.append(halfState)

    def sortHalfStates(x):
        return x.name

    halfStates = sorted(halfStates, key=sortHalfStates)

    def sortLaws(x):
        return x['State']

    laws = sorted(laws, key=sortLaws)

    for i in range(len(laws)):
        state = laws[i]['State']
        for j in range(len(halfStates)):
            if halfStates[j].name == state:

                halfStates[j].stayAtHomeOrder = laws[i]['Stay At Home Order']

                halfStates[j].mandatoryQuarantineForTravelers = laws[i][
                    'Mandatory Quarantine for Travelers']

                halfStates[j].nonEssentialBusinessClosures = laws[i][
                    'Non-Essential Business Closures']

                halfStates[j].largeGatheringsBan = laws[i][
                    'Large Gatherings Ban']

                halfStates[j].stateMandatedSchoolClosures = laws[i][
                    'State-Mandated School Closures']

                halfStates[j].barRestaurantLimits = laws[i][
                    'Bar/Restaurant Limits']

                halfStates[j].primaryElectionPostponement = laws[i][
                    'Primary Election Postponement']

                halfStates[j].emergencyDeclaration = laws[i][
                    'Emergency Declaration']

                halfStates[j].numberOfLaws = laws[i]['Number of Laws']

    for state in halfStates:
        newState = State(
            state_name=state.name,
            positive=state.positive,
            negative=state.negative,
            hospitalized=state.hospitalized,
            deaths=state.deaths,
            stay_at_home_order=state.stayAtHomeOrder,
            mandatory_quarantine_for_travelers=state.
            mandatoryQuarantineForTravelers,
            non_essential_business_closures=state.nonEssentialBusinessClosures,
            large_gatherings_ban=state.largeGatheringsBan,
            state_mandated_school_closures=state.stateMandatedSchoolClosures,
            bar_restaurant_limits=state.barRestaurantLimits,
            primary_election_postponement=state.primaryElectionPostponement,
            emergency_declaration=state.emergencyDeclaration,
            number_of_laws=state.numberOfLaws)

        # Sanity check to make sure our data makes sense for sorting purposes

        if newState.stay_at_home_order == '-':
            newState.stay_at_home_order= None

        if newState.mandatory_quarantine_for_travelers == '-':
            newState.mandatory_quarantine_for_travelers = None

        if newState.non_essential_business_closures == '-':
            newState.non_essential_business_closures = None

        if newState.large_gatherings_ban == '-':
            newState.large_gatherings_ban = None

        if newState.state_mandated_school_closures == '-':
            newState.state_mandated_school_closures = None

        if newState.bar_restaurant_limits == '-':
            newState.bar_restaurant_limits = None

        if newState.primary_election_postponement == '-':
            newState.primary_election_postponement = None

        if newState.emergency_declaration == '-':
            newState.emergency_declaration = None

        if newState.number_of_laws == None:
            newState.number_of_laws = 0
        # After I create the state, I can then add it to my session.
        db.session.add(newState)
    # commit the session to my DB.
    db.session.commit()


def create_counties():
    counties = load_json("countydata.json")

    for county in counties:
        name = county['county']
        state = county['state']
        #If the fips are none replace it with a 0 for reporting
        if county['fips'] == None:
            fips = 0
        else:
            fips = county['fips']
        cases = county['cases']
        deaths = county['deaths']

        newCounty = County(county_name=name,
                           state=state,
                           deaths=deaths,
                           cases=cases,
                           fips=fips)
        # After I create the county, I can then add it to my session.
        db.session.add(newCounty)
    # commit the session to my DB.
    db.session.commit()

with open("allstatesdata.json", "wb") as f:
    response = requests.get('https://covidtracking.com/api/v1/states/daily.json')
    f.write(response.content)

with open('allstatesdata.json') as f:
  data = json.load(f)

recent = data[0]['date']

todaydata = []
for x in data:
    if x['date'] == recent:
        todaydata.append(x)

statenames = {'AK':'Alaska', 'AL':'Alabama', 'AR':'Arkansas', 'AS':'American Samoa', 'AZ':'Arizona', 'CA':'California', 'CO':'Colorado', 'CT':'Connecticut', 'DC':'District of Columbia', 'DE':'Delaware', 'FL':'Florida', 'GA':'Georgia', 'GU':'Guam', 'HI':'Hawaii', 'IA':'Iowa', 'ID':'Idaho', 'IL':'Illinois', 'IN':'Indiana', 'KS':'Kansas', 'KY':'Kentucky', 'LA':'Louisiana', 'MA':'Massachusetts', 
'MD':'Maryland', 'ME':'Maine', 'MI':'Michigan', 'MN':'Minnesota', 'MO':'Missouri', 'MP':'Northern Mariana Islands', 'MS':'Mississippi', 'MT':'Montana', 'NC':'North Carolina', 'ND':'North Dakota', 'NE':'Nebraska', 'NH':'New Hampshire', 'NJ':'New Jersey', 'NM':'New Mexico', 'NV':'Nevada', 'NY':'New York', 'OH':'Ohio', 'OK':'Oklahoma', 'OR':'Oregon', 'PA':'Pennsylvania', 'PR':'Puerto Rico', 'RI':'Rhode Island', 'SC':'South Carolina', 'SD':'South Dakota', 'TN':'Tennessee', 'TX':'Texas', 'UT':'Utah', 'VA':'Virginia', 'VI':'Virgin Islands', 'VT':'Vermont', 'WA':'Washington', 'WI':'Wisconsin', 'WV':'West Virginia', 'WY':'Wyoming'}

for x in todaydata:
  name=x['state']
  x['state'] = statenames[name]

with open('statesdata.json', 'w') as f:
    json.dump(todaydata, f)

with open("allcountydata.csv", "wb") as f:
    response = requests.get('https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-counties.csv')
    f.write(response.content)

with open('allcountydata.csv', 'r') as csvdata:
    next(csvdata, None) # skip the headers
    reader = csv.DictReader(csvdata,fieldnames=['date','county','state','fips','cases','deaths'])
    json.dump([row for row in reader], open('allcountydata.json', 'w+'))

with open('allcountydata.json') as f:
  cdata = json.load(f)

crecent = cdata[-1]['date']

todaycdata = []
for x in cdata:
    if x['date'] == crecent:
        todaycdata.append(x)

for x in todaycdata:
  try: x['fips'] = int(x['fips'])
  except ValueError:
    x['fips'] = None
  x['cases'] = int(x['cases'])
  x['deaths'] = int(x['deaths'])

with open('countydata.json', 'w') as f:
    json.dump(todaycdata, f)


create_states()
create_counties()

# end of create_db.py
