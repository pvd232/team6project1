// About page javascript
// api key: 3wSq8qjjk1VJH3tHxida
// Calling gitlab api for information
const config = {
  headers: {
    'PRIVATE-TOKEN': '3wSq8qjjk1VJH3tHxida',
  },
};

// Counting the commits and seperating them by user
const countCommits = (data) => {
  const commitCounts = {
    peter: 0,
    pearl: 0,
    aali: 0,
    fernando: 0,
    arnab: 0,
    david: 0,
    total: data.length,
  };

  for (commit of data) {
    switch (commit.author_name) {
      case 'pvd232':
        commitCounts['peter']++;
        break;
      case 'Arnab Zubair':
        commitCounts['arnab']++;
        break;
      case 'Fernando Martinez Mojica':
        commitCounts['fernando']++;
        break;
      case 'Dung Le':
        commitCounts['pearl']++;
        break;
      case 'Aali Sahay':
        commitCounts['aali']++;
        break;
      default:
        commitCounts['david']++;
        break;
    }
  }

  return commitCounts;
};

// Counting the issues
const countIssues = (data) => {
  const issueCounts = {
    peter: 0,
    pearl: 0,
    aali: 0,
    fernando: 0,
    arnab: 0,
    david: 0,
    total: data.length,
  };

  for (issue of data) {
    switch (issue.author.name) {
      case 'pvd232':
        issueCounts['peter']++;
        break;
      case 'Arnab Zubair':
        issueCounts['arnab']++;
        break;
      case 'Fernando Martinez Mojica':
        issueCounts['fernando']++;
        break;
      case 'Dung Le':
        issueCounts['pearl']++;
        break;
      case 'Aali Sahay':
        issueCounts['aali']++;
        break;
      case 'David Alvarez':
        issueCounts['david']++;
        break;
      default:
        break;
    }
  }

  return issueCounts;
};

// Adding the commits to the page
const addCommitsToPage = (data) => {
  document.getElementById(
      'peter_commits',
  ).innerHTML = `<strong>No of Commits:</strong> ${data['peter']}`;

  document.getElementById(
      'arnab_commits',
  ).innerHTML = `<strong>No of Commits:</strong> ${data['arnab']}`;

  document.getElementById(
      'fernando_commits',
  ).innerHTML = `<strong>No of Commits:</strong> ${data['fernando']}`;

  document.getElementById(
      'pearl_commits',
  ).innerHTML = `<strong>No of Commits:</strong> ${data['pearl']}`;

  document.getElementById(
      'aali_commits',
  ).innerHTML = `<strong>No of Commits:</strong> ${data['aali']}`;

  document.getElementById(
      'david_commits',
  ).innerHTML = `<strong>No of Commits:</strong> ${data.david}`;

  document.getElementById(
      'total_commits',
  ).innerHTML = `<strong>Total no. of Commits:</strong> ${data['total']}`;
};

// Adding the issues to the page
const addIssuesToPage = (data) => {
  document.getElementById(
      'peter_issues',
  ).innerHTML = `<strong>No of Issues:</strong> ${data['peter']}`;

  document.getElementById(
      'arnab_issues',
  ).innerHTML = `<strong>No of Issues:</strong> ${data['arnab']}`;

  document.getElementById(
      'fernando_issues',
  ).innerHTML = `<strong>No of Issues:</strong> ${data['fernando']}`;

  document.getElementById(
      'pearl_issues',
  ).innerHTML = `<strong>No of Issues:</strong> ${data['pearl']}`;

  document.getElementById(
      'aali_issues',
  ).innerHTML = `<strong>No of Issues:</strong> ${data['aali']}`;

  document.getElementById(
      'david_issues',
  ).innerHTML = `<strong>No of Issues:</strong> ${data['david']}`;

  document.getElementById(
      'total_issues',
  ).innerHTML = `<strong>Total no. of Issues:</strong> ${data['total']}`;
};

// Calling commits
async function commitsCall() {
  const [firstPage, secondPage] = await Promise.all([
    axios
        .get(
            'https://gitlab.com/api/v4/projects/16850877/repository/commits?per_page=100',
            config,
        ),
    axios
        .get(
            'https://gitlab.com/api/v4/projects/16850877/repository/commits?per_page=100&page=2',
            config,
        ),
  ],
  );
  const commits = firstPage.data.concat(secondPage.data);
  addCommitsToPage(countCommits(commits));
}

commitsCall();
// Api call to get data for issues
axios
    .get(
        'https://gitlab.com/api/v4/projects/16850877/issues?per_page=100',
        config,
    )
    .then((res) => {
      addIssuesToPage(countIssues(res.data));
    })
    .catch((err) => {
      console.log(err);
    });


