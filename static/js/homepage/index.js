// Changes the date number to a string and give it formatting
const dateToString = date => {
  let newDate = date.toString().split("");
  let day = newDate.splice(6, 7).join("");
  let month = newDate.splice(4, 5).join("");
  let year = newDate.splice(0, 4).join("");
  return month + "/" + day + "/" + year;
};

//Calling the covid api and getting its data
axios
  .get("https://covidtracking.com/api/us/daily")
  .then(res => {
    var totalCaseChart = document.getElementById("totalCases");
    const labels = [];
    const data = [];
    res.data.forEach(current => {
      labels.unshift(dateToString(current.date));
      data.unshift(current.total);
    });

    var myChart = new Chart(totalCaseChart, {
      type: "line",
      data: {
        labels: labels,
        datasets: [
          {
            label: "Coronavirus Cases in the US",
            data: data,
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            borderColor: [
              "rgba(255, 99, 132, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)"
            ],
            borderWidth: 1
          }
        ],
        options: {
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true
                }
              }
            ]
          }
        }
      }
    });

    // Code for the total death chart

    var totalDeathChart = document.getElementById("totalDeaths");
    const deathData = [];

    res.data.forEach(current => {
      if (current.death === null) {
        deathData.unshift(0);
      } else {
        deathData.unshift(current.death);
      }
    });

    var deathChart = new Chart(totalDeathChart, {
      type: "line",
      data: {
        labels: labels,
        datasets: [
          {
            label: "Coronavirus Deaths in the US",
            data: deathData,
            backgroundColor: [
              "rgba(44, 130, 201, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            borderColor: [
              "rgba(34, 49, 63, 1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)"
            ],
            borderWidth: 1
          }
        ],
        options: {
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true
                }
              }
            ]
          }
        }
      }
    });
  })
  .catch(err => {
    console.log(err);
  });
