google.charts.load('current', {
    'packages':['geochart'],
    // Note: you will need to get a mapsApiKey for your project.
    // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
    'mapsApiKey': 'AIzaSyCwk5fuayMJ3ptIxHt2Ekq7jEQW2xjAzOY'
  });
  google.charts.setOnLoadCallback(drawRegionsMap);

  function drawRegionsMap() {
    var data = google.visualization.arrayToDataTable([
      ['State', 'Select'],
      {% for state in zip(list(USdata.keys()),list(USdata.values())): %}
        [{{list(state)}}],
        {% endfor %}

    ]);

    var options = {
      region: 'US', 
      displayMode: 'regions',
      resolution: 'provinces',
      colorAxis: {colors: ['#00853f', 'black', '#e31b23']},
      backgroundColor: '#81d4fa',
      datalessRegionColor: '#f8bbd0',
      defaultColor: '#f5f5f5',
    };

    var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

    chart.draw(data, options);
  }