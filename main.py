import subprocess
from flask import Flask, render_template, request, redirect, url_for, jsonify
import json
import random

# Frontend imports
from create_db import app, db, County, State

#app = Flask(__name__)


@app.route('/')
@app.route('/splash')
def splash():

    return render_template('splash.html')


@app.route('/home')
def home():

    return render_template('index.html')


@app.route('/states')
def states():
    page = request.args.get('page', 1, type=int)
    sort = request.args.get('sort', 'asc', type=str)
    attr = request.args.get('attr', 'state_name', type=str)
    states = db.session.query(State).order_by(getattr(State, attr).asc()).paginate(per_page=10, page=page)

    if sort == 'asc':
        states = db.session.query(State).order_by(getattr(State, attr).asc()).paginate(per_page=10, page=page)

    else:
        states = db.session.query(State).order_by(getattr(State, attr).desc()).paginate(per_page=10, page=page)

    return render_template('stateInfo.html',
                           states=states,
                           sort=sort,
                           attr=attr)


@app.route('/visualize')
def dataVis():
    with open("statesdata.json", "r") as f:
        sdata = f.read()
        data = json.loads(sdata)
    us_data = {}
    for state in data:
        us_data[state['state']] = state['positive']

    return render_template('Visualize.html', USdata=us_data)


@app.route('/counties')
def counties():
    page = request.args.get('page', 1, type=int)
    sort = request.args.get('sort', 'asc', type=str)
    attr = request.args.get('attr', 'county_name', type=str)
    counties = db.session.query(County).order_by(
        getattr(County, attr).asc()).paginate(per_page=10, page=page)

    # This sorts out what order to put the information in
    if sort == 'asc':
        counties = db.session.query(County).order_by(
            getattr(County, attr).asc()).paginate(per_page=10, page=page)
    else:
        counties = db.session.query(County).order_by(
            getattr(County, attr).desc()).paginate(per_page=10, page=page)

    return render_template('counties.html',
                           counties=counties,
                           sort=sort,
                           attr=attr)


@app.route('/laws')
def laws():
    page = request.args.get('page', 1, type=int)
    sort = request.args.get('sort', 'asc', type=str)
    attr = request.args.get('attr', 'state_name', type=str)
    states = db.session.query(State).paginate(per_page=10, page=page)
    if sort == 'asc' and attr:
        states = db.session.query(State).order_by(getattr(State, attr).asc()).paginate(per_page=10, page=page)
    else:
        states = db.session.query(State).order_by(getattr(State, attr).desc()).paginate(per_page=10, page=page)

    return render_template('laws.html', states=states, sort=sort, attr=attr)


@app.route('/about')
def about():

    return render_template('about.html')


@app.route('/unittest')
def unittest():
    try:
        subprocess.call(["python", "tests.py"])
        subprocess.call(["python", "models.py"])
        subprocess.call(["python", "create_db.py"])
        result = "All tests successfully passed!"
        return render_template('unittest.html', result=result)
    except:
        result = "Some tests failed."
        return render_template('unittest.html', result=result)


# added in a path parameter so the url displays the current state that is being displayed in the stateDetail page
@app.route('/<string:stateParam>Detail')
def stateDetails(stateParam):
    stateParam = stateParam
    with open('allcountydata.json') as f:
        cdata = json.load(f)
        crecent = cdata[-1]['date']
    # queried the database to get the specific state that we want to display in the state detail page. the query automatically returns a list so I grabbed the first element
    state = db.session.query(State).filter(
        State.state_name.ilike(stateParam))[0]
    numcounties = db.session.query(County).filter(
        County.state == state.state_name).count()
    numlaws = db.session.query(State.number_of_laws).filter(
        State.state_name == state.state_name)[0][0]
    worstcount = db.session.query(County.county_name).filter(
        County.state == state.state_name).order_by(County.cases)[-1][0]
    return render_template('stateDetails.html', state=state, numcounties=numcounties, numlaws=numlaws, worstcount=worstcount, crecent=crecent)


@app.route('/CountyDetail')
def countyDetails():
    stateParam = request.args.get('stateParam', None) 
    countyParam = request.args.get('countyParam', None)
    # queried the database to get the specific county that we want to display in the state detail page. the query automatically returns a list so I grabbed the first element
    county = db.session.query(County).filter_by(county_name = countyParam, state = stateParam)[0]
    return render_template('county_detail.html', county=county)


@app.route('/search_res', methods=["GET", "POST"])
def search_res():
    if request.method == "POST":
        term = request.form["term"].capitalize()
        states = db.session.query(State).filter(State.state_name == term).all()
        counties = db.session.query(County).filter(
            County.county_name == term).all()
        county_state = db.session.query(
            County).filter(County.state == term).all()
        return render_template('search_results.html', states=states, counties=counties, county_state=county_state)


@app.route('/search')
def search():
    return render_template('search.html')


if __name__ == '__main__':
    app.debug = True
    app.run()
