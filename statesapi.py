import requests
from datetime import datetime
import json

# today = datetime.today().strftime('%Y%m%d')
# today = int(today)
# yesterday = today-1
# print(yesterday)

with open("allstatesdata.json", "wb") as f:
    response = requests.get('https://covidtracking.com/api/v1/states/daily.json')
    f.write(response.content)

with open('allstatesdata.json') as f:
  data = json.load(f)

recent = data[0]['date']
# print(recent)

todaydata = []
for x in data:
    if x['date'] == recent:
        todaydata.append(x)

#print(todaydata)

statenames = {'AK':'Alaska', 'AL':'Alabama', 'AR':'Arkansas', 'AS':'American Samoa', 'AZ':'Arizona', 'CA':'California', 'CO':'Colorado', 'CT':'Connecticut', 'DC':'District of Columbia', 'DE':'Delaware', 'FL':'Florida', 'GA':'Georgia', 'GU':'Guam', 'HI':'Hawaii', 'IA':'Iowa', 'ID':'Idaho', 'IL':'Illinois', 'IN':'Indiana', 'KS':'Kansas', 'KY':'Kentucky', 'LA':'Louisiana', 'MA':'Massachusetts', 
'MD':'Maryland', 'ME':'Maine', 'MI':'Michigan', 'MN':'Minnesota', 'MO':'Missouri', 'MP':'Northern Mariana Islands', 'MS':'Mississippi', 'MT':'Montana', 'NC':'North Carolina', 'ND':'North Dakota', 'NE':'Nebraska', 'NH':'New Hampshire', 'NJ':'New Jersey', 'NM':'New Mexico', 'NV':'Nevada', 'NY':'New York', 'OH':'Ohio', 'OK':'Oklahoma', 'OR':'Oregon', 'PA':'Pennsylvania', 'PR':'Puerto Rico', 'RI':'Rhode Island', 'SC':'South Carolina', 'SD':'South Dakota', 'TN':'Tennessee', 'TX':'Texas', 'UT':'Utah', 'VA':'Virginia', 'VI':'Virgin Islands', 'VT':'Vermont', 'WA':'Washington', 'WI':'Wisconsin', 'WV':'West Virginia', 'WY':'Wyoming'}


for x in todaydata:
  name=x['state']
  x['state'] = statenames[name]

#print(todaydata)

with open('statesdata.json', 'w') as f:
    json.dump(todaydata, f)






