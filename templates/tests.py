import os 
import sys 
import unittest
from models import db, State, County

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
    	s = State(
    		state_name="Alabama",
    		positive=859,
    		negative=5694,
    		hospitalized=None,
    		deaths=6,
    		stay_at_home_order="-",
    		mandatory_quarantine_for_travelers = "-",
    		non_essential_business_closures = "Other" ,
    		large_gatherings_ban = ">10 People Prohibited",
    		state_mandated_school_closures = "Yes",
    		bar_restaurant_limits = "Closed except for takeout/delivery",
    		primary_election_postponement = "Yes",
    		emergency_declaration = "Yes",
    		number_of_laws = 6)
    	db.session.add(s)
    	db.session.commit()

    	r = db.session.query(State).filter_by(state_name = "Alabama").one()
    	self.assertEqual(str(r.state_name), "Alabama")

    	db.session.query(State).filter_by(state_name = "Alabama").delete()
    	db.session.commit()

    def test_source_insert_2(self):
    	s = State(
    		state_name="Alaska",
    		positive=114,
    		negative=3540,
    		hospitalized=7,
    		deaths=3,
    		stay_at_home_order="Statewide",
    		mandatory_quarantine_for_travelers = "All Travelers",
    		non_essential_business_closures = "All Non-Essential Businesses" ,
    		large_gatherings_ban = "All Gatherings Prohibited",
    		state_mandated_school_closures = "Yes",
    		bar_restaurant_limits = "Closed except for takeout/delivery",
    		primary_election_postponement = "-",
    		emergency_declaration = "Yes",
    		number_of_laws = 7)
    	db.session.add(s)
    	db.session.commit()

    	r = db.session.query(State).filter_by(state_name = "Alaska").one()
    	self.assertEqual(str(r.state_name), "Alaska")

    	db.session.query(State).filter_by(state_name = "Alaska").delete()
    	db.session.commit()
    
    def test_source_insert_3(self):
    	s = State(
    		state_name = "American Samoa",
    		positive = None,
    		negative = None,
    		hospitalized = None,
    		deaths=0,
    		stay_at_home_order = None,
    		mandatory_quarantine_for_travelers = None,
    		non_essential_business_closures = None ,
    		large_gatherings_ban = None,
    		state_mandated_school_closures = None,
    		bar_restaurant_limits = None,
    		primary_election_postponement = None,
    		emergency_declaration = None,
    		number_of_laws = None)
    	db.session.add(s)
    	db.session.commit()

    	r = db.session.query(State).filter_by(state_name = "American Samoa").one()
    	self.assertEqual(str(r.state_name), "American Samoa")

    	db.session.query(State).filter_by(state_name = "American Samoa").delete()
    	db.session.commit()

    def test_source_insert_4(self):
        s = State(
            state_name="Alabama",
            positive=859,
            negative=5694,
            hospitalized=None,
            deaths=6,
            stay_at_home_order="-",
            mandatory_quarantine_for_travelers = "-",
            non_essential_business_closures = "Other",
            large_gatherings_ban = ">10 People Prohibited",
            state_mandated_school_closures = "Yes",
            bar_restaurant_limits = "Closed except for takeout/delivery",
            emergency_declaration = "Yes",
            number_of_laws = 6
        )
        
        db.session.add(s)
        db.session.commit()

        c = County(
            county_name="Autauga",
            state = "Alabama",
            fips = 1001,
            cases = 7,
            deaths = 0
        )
        
        db.session.add(c)
        db.session.commit()
        
        r = db.session.query(County).filter_by(county_name = "Autauga").one()
        self.assertEqual(str(r.county_name), "Autauga")
        
        db.session.query(County).filter_by(county_name = "Autauga").delete()
        db.session.query(State).filter_by(state_name="Alabama").delete()
        db.session.commit()
    
    def test_source_insert_5(self):
        s = State(
            state_name="Alabama",
            positive=859,
            negative=5694,
            hospitalized=None,
            deaths=6,
            stay_at_home_order="-",
            mandatory_quarantine_for_travelers = "-",
            non_essential_business_closures = "Other",
            large_gatherings_ban = ">10 People Prohibited",
            state_mandated_school_closures = "Yes",
            bar_restaurant_limits = "Closed except for takeout/delivery",
            emergency_declaration = "Yes",
            number_of_laws = 6
        )
        
        db.session.add(s)
        db.session.commit()

        c = County(
    		county_name = "Baldwin",
    		state = "Alabama",
    		fips = 1003,
    		cases = 1003,
    		deaths = 0)
        db.session.add(c)
        db.session.commit()

        r = db.session.query(County).filter_by(county_name = "Baldwin").one()
        self.assertEqual(str(r.county_name), "Baldwin")

        db.session.query(County).filter_by(county_name = "Baldwin").delete()
        db.session.query(State).filter_by(state_name="Alabama").delete()
        db.session.commit()
    
    def test_source_insert_6(self):
        s = State(
            state_name="Alabama",
            positive=859,
            negative=5694,
            hospitalized=None,
            deaths=6,
            stay_at_home_order="-",
            mandatory_quarantine_for_travelers = "-",
            non_essential_business_closures = "Other",
            large_gatherings_ban = ">10 People Prohibited",
            state_mandated_school_closures = "Yes",
            bar_restaurant_limits = "Closed except for takeout/delivery",
            emergency_declaration = "Yes",
            number_of_laws = 6
        )
        
        db.session.add(s)
        db.session.commit()

        c = County(
    		county_name = "Bibb",
    		state = "Alabama",
    		fips = None,
    		cases = None,
    		deaths = None
        )
        db.session.add(c)
        db.session.commit()

        r = db.session.query(County).filter_by(county_name = "Bibb").one()
        self.assertEqual(str(r.county_name), "Bibb")

        db.session.query(County).filter_by(county_name = "Bibb").delete()
        db.session.query(State).filter_by(state_name="Alabama").delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()